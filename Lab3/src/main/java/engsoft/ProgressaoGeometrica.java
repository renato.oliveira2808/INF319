package engsoft;

public class ProgressaoGeometrica extends Progressao {
	
	int base;

	public ProgressaoGeometrica(int base) {
		this.base = base;
		this.qtdTermos = 0;
		// TODO Auto-generated constructor stub
	}

	public ProgressaoGeometrica() {
		this.base = 2;
		this.qtdTermos = 0;
		// TODO Auto-generated constructor stub
	}

	@Override
	public int inicia() {
		termos = new int[]{1};
		this.qtdTermos++;
		return 1;
	}

	@Override
	public int proxTermo() {
		int[] temp = new int[qtdTermos +1];
		
		for (int i = 0; i < qtdTermos; i++){
		      temp[i] = termos[i];
		}
		
		temp[qtdTermos] = termos[ qtdTermos -1 ]*base;
		termos = temp;
		qtdTermos++;
		return termos[qtdTermos -1];
	}

	@Override
	public int iesimoTermo(int i) {
		int j =1;
		
		while(i > 0) {
			j = j*base;
			i--;
		}
		return j;
	}

	@Override
	public String imprimeProgressao(int fim) {
		StringBuilder  list = new StringBuilder();
		Integer j = 1;
		
		for (int i = 0; i < fim; i++) {
			list.append(j.toString() + " ");
			j = j*base;
		}

		list.append(j.toString() + "\n");
		return list.toString();
	}

}