package engsoft;

public class ProgressaoAritmetica extends Progressao {
	
	int incremento;

	public ProgressaoAritmetica(int incremento) {
		this.incremento = incremento;
		this.qtdTermos = 0;
		// TODO Auto-generated constructor stub
	}

	public ProgressaoAritmetica() {
		this.incremento = 1;
		this.qtdTermos = 0;
		// TODO Auto-generated constructor stub
	}

	@Override
	public int inicia() {
		termos = new int[]{0};
		this.qtdTermos++;
		return 0;
	}

	@Override
	public int proxTermo() {
		int[] temp = new int[qtdTermos +1];
		
		for (int i = 0; i < qtdTermos; i++){
		      temp[i] = termos[i];
		}
		
		temp[qtdTermos] = termos[ qtdTermos -1 ] + incremento;
		termos = temp;
		qtdTermos++;
		return termos[qtdTermos -1];
	}

	@Override
	public int iesimoTermo(int i) {
		int j =0;
		
		while(i > 0) {
			j = j + incremento;
			i--;
		}
		return j;
	}

	@Override
	public String imprimeProgressao(int fim) {
		StringBuilder  list = new StringBuilder();
		Integer j = 0;
		
		for (int i = 0; i < fim; i++) {
			list.append(j.toString() + " ");
			j = j+incremento;
		}

		list.append(j.toString() + "\n");
		return list.toString();
	}

}
