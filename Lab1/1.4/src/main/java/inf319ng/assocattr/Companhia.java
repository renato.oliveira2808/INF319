package inf319ng.assocattr;

import java.util.HashSet;
import java.util.Set;

public class Companhia {

	private String nome_companhia;
	private Set<Contrato> contratos;
	
	public Companhia() {
		nome_companhia = "";
		contratos = new HashSet<>();
	}
	
	public Companhia(String nome_companhia) {
		this.nome_companhia = nome_companhia;
		contratos = new HashSet<>();
	}
	
	public String getNome() {
		return nome_companhia;
	}
	
	public int getNumeroEmpregados() {
		return contratos.size();
	}
	
	public void emprega(Pessoa p1, Double s) {
		Contrato contrato = new Contrato(this, p1, s);
		p1.emprega(contrato);
		contratos.add(contrato);
	}
	
	public void demite(Pessoa empregado) {
		if (contratos.size() > 0) {
			contratos.removeIf(c -> c.getEmpregado() == empregado);
			empregado.demite();
		}
	}
	
	public Double custoTotal() {
		if (contratos.size() > 0)
			return contratos.stream().mapToDouble(Contrato::getSalario).sum();
		else 
			return 0.0;
	}
	
}