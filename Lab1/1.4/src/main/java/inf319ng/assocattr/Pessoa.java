package inf319ng.assocattr;

public class Pessoa {
	
	private String nome;
	private String sobrenome;
    private Contrato contrato;

    public Pessoa() {
    	nome = "";
    	sobrenome = "";
    	contrato = null;
    }
    
    public Pessoa(String nome, String sobrenome) {
    	this.nome = nome;
    	this.sobrenome = sobrenome;
    	contrato = null;
    }
      
    public String getNome() {
    	return nome;
    }
    
    public String getSobrenome() {
    	return sobrenome;
    }
    
    public void setNome(String nome) {
    	this.nome = nome;
    }
    
    public void setSobrenome(String sobrenome) {
    	this.sobrenome = sobrenome;
    }
    
    public Companhia getEmprego() {
    	if (this.contrato != null)
    		return this.contrato.getEmpregador();
    	else 
    		return null;
    }
    
    public Double getSalario() {
    	if (this.contrato != null)
    		return this.contrato.getSalario();
    	else 
    		return null;
    }
    
    public void emprega(Contrato c) {
        this.contrato = c; 
    }

    public void demite() {
        this.contrato = null;
    }

}