package inf319ng.assocbid;

import java.util.HashSet;
import java.util.Set;

public class Companhia {

	private String nome_companhia;
	private int contratos;
	private Set<Pessoa> empregados;
	
	public Companhia() {
		nome_companhia = "";
		empregados = new HashSet<>();
		contratos = 0;
	}
	
	public Companhia(String nome_companhia) {
		this.nome_companhia = nome_companhia;
		empregados = new HashSet<>();
		contratos = 0;
	}
	
	public String getNome() {
		return nome_companhia;
	}
	
	public int getNumeroEmpregados() {
		return contratos;
	}
	
	public void emprega(Pessoa p1, Double s) {
		p1.setSalario(s);
		empregados.add(p1);
		contratos++;
	}
	
	public void demite(Pessoa empregado) {
		if (contratos > 0) {
			empregados.remove(empregado);
			empregado.setSalario(0.0);
			contratos--;
		}
	}
	
	public Double custoTotal() {
		if (contratos > 0)
			return empregados.stream().mapToDouble(Pessoa::getSalario).sum();
		else 
			return 0.0;
	}
	
}