package inf319ng.assocbid;

public class Pessoa {
	
	private String nome;
	private String sobrenome;
    private double salario;
    private Companhia emprego;

    public Pessoa() {
    	nome = "";
    	sobrenome = "";
        salario = 0.0;
    }
    
    public Pessoa(String nome, String sobrenome) {
    	this.nome = nome;
    	this.sobrenome = sobrenome;
    	this.salario = 0.0;    	
    }
    
    public Pessoa(String nome, String sobrenome, Companhia emprego, double salario) {
    	this.nome = nome;
    	this.sobrenome = sobrenome;
    	this.salario = salario;
    }
    
    public String getNome() {
    	return nome;
    }
    
    public String getSobrenome() {
    	return sobrenome;
    }
    
    public double getSalario() {
        return salario;
    }
    
    public void setSalario(Double s) {
        this.salario = s;
    }
    
    public Companhia getEmprego() {
    	return emprego;
    }
    
    /**
     * <p> O método <b>econtratado</b> estabelece a associação entre uma <b>Pessoa</b> e uma <b>Companhia</b>.</p>
     * 
     * @param empresa: nome da empresa onde a Pessoa se contratou.
     * @param salario: salario da Pessoa.
     */
    public void emprega(Companhia empresa, double salario) {
        this.emprego = empresa;
        empresa.emprega(this, salario);      
    }
    /**
     * <p>O método <b>demitese</b> remove a associação entre uma <b>Pessoa</b> e uma <b>Companhia</b>. </p>
     * 
     * <p>O método <b>demitese</b> garante que o salário é zerado e que o relacionamento entre a Pessoa e a Empresa é removido.</p>
     */
    public void demite() {
        emprego.demite(this);
        emprego = null;
    }

}
