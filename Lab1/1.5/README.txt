Nesse exercício, a classe ContratosDeTrabalho é adicionada e a mesma é associada a Contratos de modo que ContratosDeTrabalho contém dois conjuntos:

	- CompanhiasxContratos - mapeia todos os contratos de trabalho de determinada companhia.
	- EmpregadoxContratos - mapeia todos os contratos de trabalho de determinada pessoa.
	
Com essa arquitetura, cada Pessoa pode estar associada a N contratos de trabalho e cada companhia também. Assim, toda a database de contratos e seu mapeamento fica concentrada na Classe ContratosDeTrabalho em seus dois conjuntos de mapeamentos: contratosEmpregados e contratosEmpregadores.