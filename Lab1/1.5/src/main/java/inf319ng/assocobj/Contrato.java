package inf319ng.assocobj;

public class Contrato {
	
	private Companhia empregador;
	private Pessoa empregado;
	private Double salario;
	
	public Contrato(Companhia empregador, Pessoa empregado, Double salario) {
		this.empregador = empregador;
		this.empregado = empregado;
		this.salario = salario;
	}
	
	public Companhia getEmpregador(){
		return this.empregador;
	}
	
	public Pessoa getEmpregado(){
		return this.empregado;
	}
	
	public Double getSalario(){
		return this.salario;
	}
}

