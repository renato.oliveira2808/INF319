package inf319ng.assocobj;

public class Companhia {

	private String nome_companhia;
	private ContratosDeTrabalho contratos;

	public Companhia(String nome_companhia) {
		this.nome_companhia = nome_companhia;
	}
	
	public String getNome() {
		return nome_companhia;
	}
	
    public void setContratos(ContratosDeTrabalho ct) {
    	contratos = ct;
    }
    
    public ContratosDeTrabalho getContratos() {
    	return contratos;
    }
	
	public void emprega(Pessoa p1, Double s) {
		if (contratos != null) {
			contratos.emprega(this, p1, s);
		}
		else {
			contratos = new ContratosDeTrabalho();
			contratos.emprega(this, p1, s);
		}	
	}
	
	public void demite(Pessoa empregado) {
		if (contratos != null) {
			contratos.demite(this, empregado);
		}
	}
	
	public Double custoTotal() {
		if (contratos != null) {
			return contratos.getEmpregados(this).stream().mapToDouble(Contrato::getSalario).sum();
		}
		else 
			return 0.0;
	}
	
}