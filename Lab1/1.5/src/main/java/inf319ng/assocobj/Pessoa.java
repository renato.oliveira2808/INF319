package inf319ng.assocobj;

import java.util.Set;
import java.util.stream.Collectors;

public class Pessoa {
	
	private String nome;
	private String sobrenome;
    private ContratosDeTrabalho contratos;

    public Pessoa(String nome, String sobrenome) {
    	this.nome = nome;
    	this.sobrenome = sobrenome;
    }
      
    public String getName() {
    	return nome + " " + sobrenome;
    }
    
    public void setContratos(ContratosDeTrabalho ct) {
    	contratos = ct;
    }
    
    public ContratosDeTrabalho getContratos() {
    	return contratos;
    }
    
    public Set<Companhia> getEmpregos() {
    	if (contratos != null)
    		return contratos.getEmpregadores(this).stream().map(Contrato::getEmpregador).collect(Collectors.toSet());
    	else
    		return null;
    }
    
    public Double getSalarioTotal() {
    	if (contratos != null)
    		return contratos.getEmpregadores(this).stream().mapToDouble(Contrato::getSalario).sum();
    	else 
    		return 0.0;
    }

}
