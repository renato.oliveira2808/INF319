package inf319ng.assocobj;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ContratosDeTrabalho {
	
	private Map<Pessoa, HashSet<Contrato>> contratosEmpregados;
	private Map<Companhia, HashSet<Contrato>> contratosEmpregadores;

	public ContratosDeTrabalho() {
		contratosEmpregados = new HashMap<Pessoa, HashSet<Contrato>>();
		contratosEmpregadores = new HashMap<Companhia, HashSet<Contrato>>();
	}
	
	public Set<Contrato> getEmpregadores (Pessoa p) {
		return contratosEmpregados.get(p);
	}
	
	public Set<Contrato> getEmpregados (Companhia c) {
		return contratosEmpregadores.get(c);
	}
	
	public void emprega(Companhia c, Pessoa p, Double s) {
		Contrato contrato = new Contrato(c, p, s);
		if (contratosEmpregadores.get(c) != null) {
			contratosEmpregadores.get(c).add(contrato);
		}
		else {
			HashSet<Contrato> cs = new HashSet<Contrato>();
			cs.add(contrato);
			contratosEmpregadores.put(c, cs);
		}	
		
		if (contratosEmpregados.get(p) != null) {
			contratosEmpregados.get(p).add(contrato);
		}
		else {
			HashSet<Contrato> cp = new HashSet<Contrato>();
			cp.add(contrato);
			contratosEmpregados.put(p, cp);
		}
	}
	
	public void demite(Companhia c, Pessoa p) {
		if (contratosEmpregados.get(p) != null) {
			contratosEmpregados.get(p).removeIf(k -> k.getEmpregador() == c);
			contratosEmpregadores.get(c).removeIf(k -> k.getEmpregado() == p);
		}
	}
}
