package assoccp;

public class Pessoa {
	
	private String nome;
	private String sobrenome;
    private double salario;

    public Pessoa() {
    	nome = "";
    	sobrenome = "";
        salario = 0.0;
    }
    
    public Pessoa(String nome, String sobrenome) {
    	this.nome = nome;
    	this.sobrenome = sobrenome;
    	this.salario = 0.0;    	
    }
    
    public Pessoa(String nome, String sobrenome, Companhia emprego, double salario) {
    	this.nome = nome;
    	this.sobrenome = sobrenome;
    	this.salario = salario;
    }
    
    public String getNome() {
    	return nome;
    }
    
    public String getSobrenome() {
    	return sobrenome;
    }
    
    public double getSalario() {
        return salario;
    }
    
    public void setSalario(Double s) {
        this.salario = s;
    }

}