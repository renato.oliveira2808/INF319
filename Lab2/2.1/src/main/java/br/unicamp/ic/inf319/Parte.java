package br.unicamp.ic.inf319;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public abstract class Parte {
	private UUID uuid;
	protected Set<Parte> partes;
	protected String description;
	protected Double cost;
	
	public Parte(String description, Double cost) {
		this.uuid = UUID.randomUUID();
		this.description = description;
		this.cost = cost;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setCost(Double cost) {
		this.cost = cost;
	}
	
	private void addPart(Parte parte) {
		this.partes.add(parte);
	}
	
	public abstract Double cost();

	public UUID getUuid() {
		return uuid;
	}
	
}
