package br.unicamp.ic.inf319;

import java.util.HashSet;
import java.util.Iterator;

public class Montagem extends Parte {

	public Montagem(String description, Double cost) {
		super(description, cost);
		partes = new HashSet<Parte>();
	}
	
	public Double cost() {
		Double totalCost = 0.0;
		for (Iterator i = partes.iterator(); i.hasNext();)
			totalCost += ((Parte) i.next()).cost();
		return totalCost;
	}

}
