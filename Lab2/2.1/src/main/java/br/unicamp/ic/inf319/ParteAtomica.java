package br.unicamp.ic.inf319;

import java.util.UUID;

public class ParteAtomica extends Parte {

	public ParteAtomica(String description, Double cost) {
		super(description, cost);
	}
	
	public Double cost() {
		return this.cost;
	}
}
