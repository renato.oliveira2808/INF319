package br.unicamp.ic.inf319;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;


/**
 * <img src="./doc-files/Assembly.png" alt="Assembly">
 *
 * @author INF319
 */
public class Assembly extends Part {

    private final Set<Part> parts;
    private String list_parts;

    /**
     *
     * @param thePartNumber
     * @param theDescription
     */
    public Assembly(PartNumber thePartNumber, String theDescription) {
        super(thePartNumber, theDescription);
        parts = new HashSet<>();
        list_parts = "";
    }

    /**
     *
     * @return
     */
    @Override
    public double cost() {
        double totalCost = 0;
        // 1 - for opção
        //        for (Iterator<Part> i = parts.iterator(); i.hasNext();) {
        //            Part part = (Part) i.next();
        //            totalCost += part.cost();
        //        }
        //2 - for-loop
        //       for (Part part : parts) {
        //           totalCost += part.cost();
        //       }
        // 3 - Can use functional operations
        totalCost = parts.stream().map(part -> part.cost()).reduce(totalCost, (accumulator, _item) -> accumulator + _item);
        return totalCost;
    }

    /**
     *
     * @param thePart
     */
    public void addPart(Part thePart) {
        parts.add(thePart);
    }

    /**
     *
     * @return
     */
    public Set<Part> getParts() {
        return parts;
    }
    
    
    /**
     * <p>
     * Method that Returns the list of parts of an Assembly ordered by Part Number (cod), in the following structure:
     * <p>
     * Parte: cod; Descricao: desc; Custo: custo\n
     * <p>
	 * margin + Parte: subcod1; Descricaao: desc1; Custo: custo1\n
	 * <p>
	 * This Method calls the recursive method makeSublist(margin), which then concatenates to list_parts the sublist describing the 
	 * Assembly subparts shifted to the right by the given margin parameter.
	 * <p>
	 * @return the a String containing the details of the list of parts of an Assembly.
     */
    public String list() {
    	list_parts = list_parts.concat("Parte: " + this.getPartNumber().getNumber() + "; Descricao: " + this.getDescription() + "; Custo: " + this.cost() + "\n");
    	return makeSublist(" ");
    }
    
    /**
     * <p>
     * Recursive Method to build the sublist String of an Assembly, while the part p is still an Assembly
     * the recursion is called to concatenate to the string list_parts another set of lines until an Atomic part is reached.
     * <p>
     * @param margin that is the right margin of the sublist being concatenated, every sublevel of  the Assembly increases this margin in 1 space.
     * @return a String containing the details of the list of subparts of an Assembly 
     */
    public String makeSublist(String margin) {
    	parts.stream().sorted(Comparator.comparing(p -> ((Part) p).getPartNumber().getNumber())).forEach(p -> {
    		list_parts = list_parts.concat(margin + "Parte: "+ p.getPartNumber().getNumber() + "; Descricao: " + p.getDescription() + "; Custo: " + p.cost() + "\n");
    		if (p instanceof Assembly) {
    			list_parts = list_parts.concat(((Assembly) p).makeSublist(margin + " "));
    		}
    	});
    	return list_parts;
    }
}
